// Utilities
import * as React from "react";
import { PropertyControls, ControlType } from "framer";
import { Helmet } from "react-helmet";

// Relative Utilities
import Props from './Interface';
import { alignmentMap } from './alignment';
import { loremArray, getLorem } from './lorem';


export class G_Fonts extends React.Component<Props> {
  static defaultProps = {
    textAlign     : "center",
    color         : "#000",
    direction     : "ltr",
    fontFamily    : "Work Sans",
    lineHeight    : 1,
    fontSize      : 40,
    spacing       : 0,
    fontStyle     : "normal",
    text          : "Copy Here",
    textShadow    : false,
    verticalAlign : "center",
    weight        : "400",

    // Lorem Ipsum
    useLorem      : false,
    loremQuantity : 3,
  };

  // [NOTE] Order below matters
  static propertyControls: PropertyControls = {
    text: {
      type: ControlType.String,
      title: "Text",
      hidden(props) {
        return props.useLorem;
      }
    },
    fontFamily: {
      type: ControlType.String,
      title: "Font"
    },
    color: {
      type: ControlType.Color,
      title: "Color"
    },
    fontSize: {
      type: ControlType.Number,
      title: "Size",
      min: 0,
      max: 500
    },
    weight: {
      type: ControlType.Enum,
      title: "Weight",
      options: ["100", "200", "300", "400", "500", "600", "700", "800", "900"],
    },
    fontStyle: {
      type: ControlType.Enum,
      title: "Style",
      options: ["normal", "italic", "oblique"],
      optionTitles: ["Normal", "Italic", "Oblique"],
    },
    textAlign: {
      type: ControlType.SegmentedEnum,
      title: "Align",
      options: alignmentMap({ from: "flex" }),
      optionTitles: ["Left", "Center", "Right"],
    },
    verticalAlign: {
      type: ControlType.SegmentedEnum,
      title: "Vertical Align",
      options: alignmentMap({ from: "flex" }),
      optionTitles: ["Top", "Middle", "Bottom"],
    },
    direction: {
      type: ControlType.Enum,
      title: "Direction",
      options: ["ltr", "rtl"],
      optionTitles: ["Left to Right", "Right to Left"],
    },
    spacing: {
      type: ControlType.Number,
      title: "Spacing",
      min: -30,
      max: 30,
      step: 0.05,
    },
    lineHeight: {
      type: ControlType.Number,
      title: "Line Height",
      min: 0,
      step: 0.1,
    },
    textShadow: {
      type: ControlType.Boolean,
    },

    useLorem: {
      type: ControlType.Boolean,
      title: "Use Lorem?"
    },
    loremQuantity: {
      type: ControlType.Number,
      title: "Sentences",
      min: 1,
      max: loremArray.length,
      hidden(props) {
        return !props.useLorem;
      }
    }
  };

  render() {
    const {
      color,
      direction,
      fontFamily,
      fontSize,
      fontStyle,
      lineHeight,
    } = this.props;

    const containerStyle: React.CSSProperties = {
      overflow: "hidden",
      display: "flex",
      alignItems: `${this.props.verticalAlign}`,
      justifyContent: `${this.props.textAlign}`,
      height: "100%",
    };

    const weight = Number.parseInt(`${this.props.weight}`);
    const style: React.CSSProperties = {
      display: "block",
      margin: 0,
      padding: 0,
      fontSize: `${fontSize}px`,
      fontFamily,
      fontWeight: weight,
      fontStyle,
      letterSpacing: `${this.props.spacing + 0.0001}px`, // Hack to make it work better with flexbox
      lineHeight,
      textAlign: alignmentMap({ from: "flex", to: "text", value: this.props.textAlign }),
      color,
      direction,
      textShadow: `${this.props.textShadow ? "-0.02em 0.02em rgba(0, 0, 0, 0.2)" : "none"}`,
    };

    return (
      <>
        <Helmet>
          <link
            href={`https://fonts.googleapis.com/css?family=${this.props.fontFamily}:${weight}`}
            rel="stylesheet"
          />
        </Helmet>
        <div style={containerStyle}>
          <span style={style}>
            {
              (this.props.useLorem) ? getLorem(this.props.loremQuantity) : this.props.text
            }
          </span>
        </div>
      </>
    );
  }
}
