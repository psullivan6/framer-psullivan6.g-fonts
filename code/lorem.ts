export const loremArray = [
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  "Maecenas semper diam at est volutpat, non tempus ipsum fringilla.",
  "Duis dictum nibh sed urna consectetur fringilla.",
  "Maecenas mollis urna tincidunt pretium condimentum.",
  "Pellentesque eget dictum arcu.",
  "In ultrices dolor sit amet aliquet ullamcorper.",
  "In hac habitasse platea dictumst.",
  "Nulla non neque vulputate, pretium mauris a, fringilla ipsum.",
  "Ut venenatis nisl dui, a consequat ante aliquet non.",
  "Pellentesque ullamcorper viverra tellus nec tempor.",
  "In hac habitasse platea dictumst.",
  "Praesent suscipit neque eget leo molestie, non sodales augue rutrum."
];

export const getLorem = count => loremArray.slice(0, count);

export default {
  loremArray,
  getLorem
};
