const alignments = {
  flex: ["start", "center", "flex-end"],
  text: ["left", "center", "right"],
};

export const alignmentMap = ({ from, to = null, value = null }) => {
  if (to === null) {
    return alignments[from];
  }

  const index = alignments[from].indexOf(value);
  return alignments[to][index];
};

export default {
  alignmentMap,
};
