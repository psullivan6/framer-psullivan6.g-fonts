// Prop Types
interface Props {
  // Defaults
  width  : number;
  height : number;

  // Custom
  textAlign     : "start" | "center" | "flex-end";
  color         : string;
  direction     : "ltr" | "rtl";
  fontFamily    : string;
  lineHeight    : number;
  fontSize      : number;
  spacing       : number;
  fontStyle     : "normal" | "italic" | "oblique";
  text          : string;
  textShadow    : boolean;
  verticalAlign : "start" | "center" | "flex-end";
  weight        : "100" | "200" | "300" | "400" | "500" | "600" | "700" | "800" | "900";

  // Lorem Ipsum
  useLorem      : boolean;
  loremQuantity : number;
};

export default Props;
