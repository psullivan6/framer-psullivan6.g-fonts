# Google Web Fonts

Insert a text layout component using any [Google Web Font](https://fonts.google.com/). This Component leverages most font style properties for customizing layout and display.

## Usage

1. Install this Component
2. Drag the Component onto your Frame
3. Update the `Font` property panel input with the name directly from [Google Web Fonts](https://fonts.google.com/)
4. Update the `Text` property panel input with the desired text content
5. Customize via the other property panel inputs

## Properties

| Property     | Type                                                                                | Default         | Options                            | Description                                |
|--------------|-------------------------------------------------------------------------------------|-----------------|------------------------------------|--------------------------------------------|
align          | [`segmented-enum`](https://framer.gitbook.io/framer/components/code#segmented-enum) | "center"        | One of ["left", "center", "right"] | Text alignment within container            |
color          | [`string`](https://framer.gitbook.io/framer/components/code#string-control)         | "#000"          | -                                  | Text color                                 |
direction      | [`enum`](https://framer.gitbook.io/framer/components/code#enum-control)             | "ltr"           | One of ["ltr", "rtl"]              | Text direction                             |
fontFamily     | [`string`](https://framer.gitbook.io/framer/components/code#string-control)         | "Work Sans"     | -                                  | Google Web Font name (must be exact match) |
lineHeight     | [`number`](https://framer.gitbook.io/framer/components/code#number-control)         | `1`             | -                                  | Line height                                |
loremSentences | [`number`](https://framer.gitbook.io/framer/components/code#number-control)         | `1`             | -                                  | Quantity of lorem ipsum sentences          |
size           | [`number`](https://framer.gitbook.io/framer/components/code#number-control)         | `40`            | -                                  | Font size                                  |
spacing        | [`number`](https://framer.gitbook.io/framer/components/code#number-control)         | `0`             | -                                  | Letter spacing                             |
style          | [`string`](https://framer.gitbook.io/framer/components/code#string-control)         | "normal"        | -                                  | Font style                                 |
text           | [`string`](https://framer.gitbook.io/framer/components/code#string-control)         | "Copy Here"     | -                                  | Text copy content                          |
textShadow     | [`boolean`](https://framer.gitbook.io/framer/components/code#boolean-control)       | false           | -                                  | Disabled / Enabled default text-shadow     |
useLorem       | [`boolean`](https://framer.gitbook.io/framer/components/code#boolean-control)       | false           | -                                  | Use lorem ipsum text copy                  |
verticalAlign  | [`segmented-enum`](https://framer.gitbook.io/framer/components/code#segmented-enum) | "center"        | One of ["top", "middle", "bottom"] | Vertical text alignment in container       |
weight         | [`number`](https://framer.gitbook.io/framer/components/code#number-control)         | "400"           | -                                  | Font weight                                |                                                  

## Issues

[Create](https://bitbucket.org/psullivan6/framer-psullivan6.g-fonts/issues/new) a new issue or make a feature request.